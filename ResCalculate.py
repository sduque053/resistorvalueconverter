from tkinter import *
import time

global num


global valor
def text2color():
    c=0
    
    color=["negro","cafe","rojo","naranja","amarillo","verde","azul","violeta","gris","blanco","dorado","plateado"]
    tolera=[None,"+/- 1%","+/- 2%",None,None,"+/- 0.5%",None,None,None,None,"+/- 5%","+/- 10%"]
    a=entrada.get()
    b=list(a)
    bandas=[None]*4
    to=tolerancia.get()
    try :
        #de resis a colores
        if(len(b)==1):
            bandas[1]=color[0]
            color2.set=color[0]
            bandas[3]=color[10]
            color3.set(color[10])
            for i in range(0,12):
                try:
                    if(int(b[0])==i):
                        bandas[0]=color[i]
                        color1.set(color[i])
                    if(to==tolera[i]):
                        bandas[3]=color[i]
                        color4.set(color[i])
                except(ValueError):
                    je=Label(v_resis,text="Ingrese un dato válido").place(x=300,y=300)
            

        if(b[1]=="," or b[1]=="."):
            bandas[3]=color[10]
            color3.set(color[10])
            for i in range(0,12):
                try:
                    if(int(b[0])==i):
                        bandas[0]=color[i]
                        color1.set(color[i])
                    if(int(b[2])==i):
                        bandas[1]=color[i]
                        color2.set(color[i])
                    if(to==tolera[i]):
                        bandas[3]=color[i]
                        color4.set(color[i])
                except(ValueError):
                    je=Label(v_resis,text="Ingrese un dato válido").place(x=300,y=300)
        else:
            for j in range(2,len(b)):
                c+=1
            for i in range(0,12):
                try:
                    if(int(b[0])==i):
                        bandas[0]=color[i]
                        color1.set(color[i])
                    if(int(b[1])==i):
                        bandas[1]=color[i]
                        color2.set(color[i])
                    if(c==i):
                        bandas[2]=color[i]
                        color3.set(color[i])
                    if(to==tolera[i]):
                        bandas[3]=color[i]
                        color4.set(color[i])
                except(ValueError):
                    je=Label(v_resis,text="Ingrese un dato válido").place(x=300,y=300)
                
            
        
    except(IndexError):
        print("error")
    print(bandas)



def imprimir():
    resis=[None]*4
    resis[0]=color1.get()
    resis[1]=color2.get()
    resis[2]=color3.get()
    resis[3]=color4.get()
    color=["negro","cafe","rojo","naranja","amarillo","verde","azul","violeta","gris","blanco"]
    valores=[None]*4

    #de colores a resis 
    for j in range(0,10):#con esto hace ciclo en las dos primeras bandas
        if(resis[0]==color[j]):
            valores[0]=j
        if(resis[1]==color[j]):
            valores[1]=j
        if(resis[2]==color[j]):
            valores[2]=10**j
    if(resis[3]=="cafe"):
        valores[3]="+/- 1%"
    if(resis[3]=="rojo"):
        valores[3]="+/- 2"
    if(resis[3]=="verde"):
        valores[3]="+/- 0.5%"
    if(resis[3]=="azul"):
        valores[3]="+/- 0.25%"
    if(resis[3]=="violeta"):
        valores[3]="+/- 0.1%"
    if(resis[3]=="gris"):
        valores[3]="+/- 0.05%"
    if(resis[3]=="dorado"):
        valores[3]="+/- 5%"
    if(resis[3]=="plateado"):
            valores[3]="+/- 10%"

    #print(((10*float(valores[0])+float(valores[1]))*valores[2]),valores[3])
    try:
        
        g=((10*float(valores[0])+float(valores[1]))*valores[2]),valores[3]
        k=str(g),"Ω" #Concateno el valor que se haya introducido con el signo de ohmn

        sale=Label(v_resis,text=str(k)).place(x=250,y=100)
    except(TypeError  or ValueError):
        print ("jeje no")
    

    
    entrada.set("") #elimino lo que haya en el Entry()
    print (type(color1.get()))
    
    

    
        
            

    
        
        
    
principal = Tk() 

#principal.resizable(0,0) # No permite que se modifique el tamañano 
principal.config(bg="black") # Le da color al fondo
principal.geometry("500x500+475+125") # Cambia el tamaño de la ventana
principal.title("Resistencias")

v_resis=Toplevel(principal)
v_resis.title("Calculadora resistencias de 5 bandas")
v_resis.config(bg="black") 
v_resis.geometry("450x500+475+125") 
#v_resis.resizable(0,0)
v_resis.withdraw()

v_resis4=Toplevel(principal)
v_resis4.title("Calculadora resistencias 4 bandas")
v_resis4.config(bg="black") 
v_resis4.geometry("450x500+475+125") 
#v_resis4.resizable(0,0)
v_resis4.withdraw()



# botones en ventana principal 
bprincipal=Button(principal,text="Calculadora de resistencias de 4 bandas",command=lambda:v_resis.deiconify() or principal.withdraw()).place(x=10,y=10)
b4b=Button(principal,text="Calculadora de resistencias de 5 bandas",command=lambda:v_resis4.deiconify() or principal.withdraw()).place(x=10,y=50)


#Botones ventana resistencias de 4 bandas  
#Varibales que reciben el valor dentro de los seleccionadores 

color1=StringVar(v_resis,name="color1") # si no se le pasa el parámetro de v_resis da error para poner el color.set
color1.set("Seleccione un color:")

color2=StringVar(v_resis) 
color2.set("Seleccione un color:")

color3=StringVar(v_resis) 
color3.set("Seleccione un color:")

color4=StringVar(v_resis)
color4.set("Seleccione un color:")

#color5=StringVar(v_resis) 
#color5.set("Seleccione un color:")


#Seleccionadores
check1=OptionMenu(v_resis,color1,"negro","cafe","rojo","naranja","amarillo","verde","azul","violeta","gris","blanco").place(x=50,y=60) 
check2=OptionMenu(v_resis,color2,"negro","cafe","rojo","naranja","amarillo","verde","azul","violeta","gris","blanco").place(x=50,y=155)
check3=OptionMenu(v_resis,color3,"negro","cafe","rojo","naranja","amarillo","verde","azul","violeta","gris","blanco").place(x=50,y=250)
check4=OptionMenu(v_resis,color4,"cafe","rojo","dorado","plateado").place(x=50,y=345)
#check5=OptionMenu(v_resis,color5,"none","negro","cafe","rojo","naranja","amarillo","verde","azul","violeta","gris","blanco").place(x=50,y=440)





l1=Label(v_resis,text="Primer banda:").place(x=50,y=30)
l2=Label(v_resis,text="Segunda banda:").place(x=50,y=125)
l3=Label(v_resis,text="Tercer banda:").place(x=50,y=220)
l4=Label(v_resis,text="Cuarta banda:").place(x=50,y=315)
#l5=Label(v_resis,text="Quinta banda:").place(x=50,y=410)


l6=Label(v_resis,text="Ingrese el valor de la resistencia: ").place(x=230,y=30)
entrada=StringVar(v_resis) #Variable que recibe el valor de la caja de texto o entry 
entry=Entry(v_resis,textvariable=entrada).place(x=230,y=60)
try:
    
    calcu=Button(v_resis,text="Calcular",command=lambda: text2color() or imprimir()).place(x=250,y=350)
except(ValueError):
    print("error de dato")
tolerancia=StringVar(v_resis) 
tolerancia.set("+/- 5%")
tole=OptionMenu(v_resis,tolerancia,"+/- 0.5%","+/- 1%","+/- 2%","+/- 5%","+/- 10%").place(x=380,y=60)









#Botones ventana 5 resistencias 





principal.mainloop()  
